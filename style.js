import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  main_header: {
    top:0,
  },
  detail_view: {
    flex: 1,
    flexDirection: 'row',
  },
  card_container: {
    flex: 1,
    height:100,
  },
  card_view: {
    width: width,
  },
  all_card: {
    top:15,
    width: width - 30,
    backgroundColor: '#2089dc',
    marginLeft:15,
    marginRight:15,
    padding:5,
    flexDirection: 'row',
    borderTopLeftRadius:10,
    borderTopRightRadius:10,
    shadowOffset:{  width: .2,  height: .2,  },
    shadowColor: '#b2b2b2',
    shadowOpacity: 8.0,
  },
  sec_detail_bar: {
    flexDirection: 'row',
    top:15,
    padding:5,
    marginLeft:15,
    marginRight:15,
    width: width - 30,
    backgroundColor:'#fff',
    shadowOffset:{  width: .2,  height: .2,  },
    shadowColor: '#b2b2b2',
    shadowOpacity: 8.0,
  },
  detail_basic_text: {
    fontSize:14,
    marginTop:4,
    marginLeft:5,
    marginRight:30,
    color:'#2089dc'
  },
  status_cont: {
    flexDirection: 'row',
  }
});