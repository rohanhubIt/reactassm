/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */


import React, {Component} from 'react';
import {Alert, Platform, StyleSheet, Text, View, Image, ScrollView, Dimensions, Button} from 'react-native';
import { Card, Header, Icon } from 'react-native-elements';
import Toast, {DURATION} from 'react-native-easy-toast';
import call from 'react-native-phone-call';


var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};

var config;

export default class App extends Component<Props> {

  state = {
      results: []
  };

  constructor(props) {
    super(props);
    // this.contents = this.contents.bind(this); //add this line
  }

componentWillMount() {
  //Initialize Firebase
  const firebase = require("firebase");

  var config = {
    apiKey: "AIzaSyCiLhhPD_DaOelnCR__zbTD8-73RpvcqPg",
    authDomain: "foodsy-9d233.firebaseapp.com",
    databaseURL: "https://foodsy-9d233.firebaseio.com",
    projectId: "foodsy-9d233",
    storageBucket: "foodsy-9d233.appspot.com"
  };

  firebase.initializeApp(config);
  firebase.database().ref('Resturent').on('value', (data) => {

  jsonString = JSON.stringify(data);
  obj = JSON.parse(jsonString);

  var arrDetails = [];

    Object.keys(obj).forEach(function(k){
        var tempArr = []
        tempArr["res_name"] = obj[k].res_name;
        tempArr["distance"] = obj[k].distance;
        tempArr["rating"] = obj[k].rating;
        tempArr["likes"] = obj[k].likes;
        tempArr["location"] = obj[k].location;
        tempArr["available"] = obj[k].available;
        tempArr["tags"] = obj[k].Tags;
        tempArr["contact"] = obj[k].contact;
        tempArr["status"] = obj[k].status;
        tempArr["cost"] = obj[k].cost;
        tempArr["image"] = obj[k].image;

        arrDetails.push(tempArr);

    });
    

    this.setState({
      results: arrDetails
    });

  });
  
}

  buttonClickListener = (contact) => {
      var args = {
        number: contact,
        prompt: true 
      };

      call(args).catch(console.error);
  };

  render() {

    contents = this.state.results.map((item, i) => {

      return(
          <View key={i} style={styles.card_view} >
              <View style={styles.all_card}>
                <Text style={{marginRight:150, color:'#fff', fontSize:16}}>
                  {item.res_name}
                </Text>
                <Icon name='thumb-tack' type='font-awesome' color='#000' />                
              </View>
              <View style={styles.sec_detail_bar}>
                <Icon name='child' type='font-awesome' color='#2089dc'/>
                <Text style={styles.detail_basic_text}>{item.distance}</Text>
                <Icon name='usd' type='font-awesome' color='#2089dc'/>
                <Text style={styles.detail_basic_text}>{item.cost}</Text>
                <Icon name='star' type='font-awesome' color='#2089dc'/>
                <Text style={styles.detail_basic_text}>{item.rating}</Text>
                <Icon name='heart' type='font-awesome' color='#2089dc'/>
                <Text style={styles.detail_basic_text}>{item.likes}</Text>                                                   
              </View>
              <Card style={styles.card_container}
                image={{ uri: item.image }}
                titleStyle={{textAlign:'left', flexDirection:'row'}}
                imageStyle={{height:300}}
                featuredTitle={item.location}
                featuredSubtitle={item.available}
                >
                <View style={styles.status_cont}>
                  <Icon name='circle' type='font-awesome' color='#2089dc'/>
                  <Text style={{marginBottom: 10,marginLeft:5, marginTop:5}}>
                    {item.status}
                  </Text>
                </View>
                <Text style={{marginBottom: 10}}>
                  {item.tags}
                </Text>
                <Button key={i} title='Call' onPress={()=> this.buttonClickListener(item.contact)}  />
              </Card>
          </View>
        );
    });

    return (
      <View style={styles.container}>
        <Header style={styles.main_header} 
          leftComponent={{ icon: 'menu', color: '#fff' }}
          centerComponent={{ text: 'Foodsy App', style: { color: '#fff' } }}
          rightComponent={{ icon: 'search', color: '#fff' }}
        />
        <ScrollView horizontal={true} style={{ backgroundColor:'#F5FCFF', padding:0, margin:0 }}>
        <View style={styles.detail_view} >
            {contents}
        </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  main_header: {
    top:0,
  },
  detail_view: {
    flex: 1,
    flexDirection: 'row',
  },
  card_container: {
    flex: 1,
    height:100,
  },
  card_view: {
    width: width,
  },
  all_card: {
    top:15,
    width: width - 30,
    backgroundColor: '#2089dc',
    marginLeft:15,
    marginRight:15,
    padding:5,
    flexDirection: 'row',
    borderTopLeftRadius:10,
    borderTopRightRadius:10,
    shadowOffset:{  width: .2,  height: .2,  },
    shadowColor: '#b2b2b2',
    shadowOpacity: 8.0,
  },
  sec_detail_bar: {
    flexDirection: 'row',
    top:15,
    padding:5,
    marginLeft:15,
    marginRight:15,
    width: width - 30,
    backgroundColor:'#fff',
    shadowOffset:{  width: .2,  height: .2,  },
    shadowColor: '#b2b2b2',
    shadowOpacity: 8.0,
  },
  detail_basic_text: {
    fontSize:14,
    marginTop:4,
    marginLeft:5,
    marginRight:30,
    color:'#2089dc'
  },
  status_cont: {
    flexDirection: 'row',
  },
  horizontal:{
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 150 
  },
  hidden: {
    width: 0,
    height: 0,
  }
});